import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CrearDepartamentos {
    
    private static final String url = "jdbc:mysql://localhost/empresa";
    private static final String user = "diurno2020";
    private static final String password = "diurno2020";
    
    private static final String creaDepartamentos = 
            "CREATE TABLE departamentos("
            + "dept_no TINYINT(2) NOT NULL PRIMARY KEY,"
            + "dnombre VARCHAR(15),"
            + "loc VARCHAR(15)"
            +")";
    
    private static final String insertaDepartamentos = 
            "INSERT INTO departamentos VALUES"
            + "(10, 'CONTABILIDAD', 'SEVILLA'),"
            + "(20, 'INVESTIGACION', 'MADRID'),"
            + "(30, 'VENTAS', 'BARCELONA'),"
            +"(40, 'PRODUCCION', 'BILBAO')";
    
    private static final String creaEmpleados =
            "CREATE TABLE empleados("
            + "empe_no SMALLINT(4) UNSIGNED NOT NULL PRIMARY KEY,"
            + "apellido VARCHAR(10),"
            + "oficio VARCHAR(10),"
            + "dir SMALLINT,"
            + "fecha_alt DATE,"
            + "salario FLOAT(6,2),"
            + "comision FLOAT(6,2),"
            + "dept_no TINYINT(2) NOT NULL,"
            + "FOREIGN KEY (dept_no) REFERENCES departamentos (dept_no)"
            + ")";
    
    private static final String insertaEmpleados = 
            "INSERT INTO empleados VALUES"
            + "(1000,'AMARO','AUXILIAR',1,20100102,940.00,0.00,10),"
            + "(2000,'CASTRO','AUXILIAR',2,20100102,890.00,0.00,10),"
            + "(3000,'HERNANDEZ','CONTABLE',3,20100102,1890.00,0.00,10),"
            + "(4000,'FERNANDEZ','TEC.LAB',4,20100102,1500.00,0.00,20),"
            + "(5000,'HERMOSILLA','DIR.LAB',5,20100102,2420.00,0.00,20),"
            + "(6000,'DE ANDRES','GERENTE',6,20100102,3450.00,0.00,40),"
            + "(7000,'GOMEZ','VENDEDOR',7,20100102,1200.00,0.00,30),"
            + "(8000,'BORREGO','VENDEDOR',8,20100102,1200.00,0.00,30)";
    
    public static void main(String[] args) {
        
        Connection conexion = null;     
        Statement sentencia = null;
        
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection(url, user, password);
            sentencia = conexion.createStatement();
            //sentencia.execute(creaEmpleados);
            System.out.println("Tabla 'departamentos' creada correctamente");
            //sentencia.execute(insertaEmpleados);
            System.out.println("Nuevos departamentos insertados correctamente");
            
        } catch (Exception ex) {
            System.err.println(ex);
        } finally {
            if(sentencia != null) {
                try {
                    sentencia.close();
                    
                } catch (SQLException ex) {
                }
            }
            
            if(conexion != null) {
                try{
                    conexion.close();
                } catch (SQLException ex) {
                }
            }
        }       
    }
}