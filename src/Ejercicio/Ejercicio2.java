package Ejercicio;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author diurno2020
 */

public class Ejercicio2 {
    private static final String url = "jdbc:mysql://localhost/empresa";
    private static final String user = "diurno2020";
    private static final String password = "diurno2020";
    
    public static void main(String[] args){
        Connection conexion = null;     
        Statement sentencia = null;

        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection(url, user, password);
            sentencia = conexion.createStatement(); //Prepara el entorno para enviar sentencia SQL
            
            DatabaseMetaData metaData = conexion.getMetaData();
            ResultSet r; 
            
            //r = metaData.getTables(null, "empresa", "empleados", null);
            r = metaData.getColumns(null, "empresa", "departamentos", null); 
            
            int cuenta = 0;
            
            while(r.next()){
                cuenta++;
                System.out.println(r);
            } 
            
            System.out.println("El numero de atributos es: "+cuenta);
            
            
        }catch(Exception e){
             System.err.print(e);    
        }finally {
            if(sentencia != null) {
                try {
                    sentencia.close();                 
                } catch (SQLException ex) {
                    System.err.print(ex);
                }
            }
            
            if(conexion != null) {
                try{
                    conexion.close();
                } catch (SQLException ex) {
                     System.err.print(ex);
                }
            }
        }
    }       
}

//executeQuerey(String) SQL devuelve un unico ResultSet (Devuelve filas)
//executeUpdate(String) No devulve ResultSet, sentencia DML(Devulve el numero de filas afectadas) o DDL(Devuelve 0)
//execute() Mas de un ResultSet

//PreparedStatement -> Marcadores de posicion(placeHolder)