package Ejercicio;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EjercicioConParametrizadas {   
    private static final String url = "jdbc:mysql://localhost/empresa";
    private static final String user = "diurno2020";
    private static final String password = "diurno2020";
    
    public static void main(String[] args){
        Connection conexion = null;     
        Statement sentencia = null;

        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection(url, user, password);
            sentencia = conexion.createStatement(); //Prepara el entorno para enviar sentencia SQL
            
            
        }catch(Exception e){
             System.err.print(e);    
        }finally {
            if(sentencia != null) {
                try {
                    sentencia.close();                 
                } catch (SQLException ex) {
                    System.err.print(ex);
                }
            }
            
            if(conexion != null) {
                try{
                    conexion.close();
                } catch (SQLException ex) {
                     System.err.print(ex);
                }
            }
        }
    }   
}
